package s1;

import java.util.ArrayList;

interface I {
}

class E implements I {

    private ArrayList<H> hs = new ArrayList<>();
    private F f;
    private B b;

    public void metG(int i) {
    }
}

class H {
}

class F {
    public void metA() {
    }
}

class G {
    public void i() {
    }
}

class B extends G {
    private long t;

    public void x() {
    }

    public void display(D d) {
        System.out.println(d);
    }
}

class D {
}