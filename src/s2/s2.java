package s2;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

class Subiect2 extends JFrame {
    private JTextField jTextField;
    private String text;

    public Subiect2() throws HeadlessException {
        setSize(400, 400);
        JPanel panel = new JPanel();
        panel.setLayout(null);

        jTextField = new JTextField("");
        this.text = jTextField.getText();
        jTextField.setBounds(20, 10, 150, 100);
        panel.add(jTextField);

        JButton button = new JButton("Buton");
        button.setBounds(190, 10, 100, 50);
        panel.add(button);

        button.addActionListener(e -> Button(jTextField.getText()));

        add(panel);
        setVisible(true);
    }

    private void Button(String text) {
        try {
            FileWriter myWriter = new FileWriter("filename.txt");
            myWriter.write(text);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Subiect2 s = new Subiect2();
    }
}